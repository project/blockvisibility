<?php

/**
 * @file
 */

/**
 * The default settings form callback.
 */
function blockvisibility_admin_settings($form_state, $preset_id = NULL) {
  $form = array();
  $form['blockvisibility_remove_sidebars_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove sidebars from admin pages'),
    '#default_value' => variable_get('blockvisibility_remove_sidebars_admin', 0),
    '#description' => t('This hides the left and right sidebars on all admin pages, with the exception of the block admin page.'),
  );
  $form['blockvisibility_remove_sidebars_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove sidebars from node add and edit pages'),
    '#default_value' => variable_get('blockvisibility_remove_sidebars_node', 0),
    '#description' => t('This hides the left and right sidebars when adding or editing a node.'),
  );
  $result = db_query("SELECT title, path FROM {menu_router} WHERE tab_parent = 'node/%%' AND title != ''");
  $additional_views = array();
  while ($row = db_fetch_array($result)) {
    if (!in_array($row['path'], array('node/%/edit'))) {
      $additional_views[$row['path']] = t('!title, (!path)', array('!title' => check_plain($row['title']), '!path' => check_plain($row['path'])));
    }
  }

  $form['blockvisibility_remove_sidebars_node_other'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Remove sidebars from other related node pages'),
    '#default_value' => variable_get('blockvisibility_remove_sidebars_node_other', array()),
    '#options' => $additional_views,
    '#description' => t('This hides the left and right sidebars if the menu path matches one of the selected options.'),
  );

  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $form['blockvisibility_remove_sidebars_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Hide sidebars on these pages.'),
    '#default_value' => variable_get('blockvisibility_remove_sidebars_pages', ''),
    '#description' => $description,
  );

  $description = t('Enter PHP code between %php. Return a positive result (1, TRUE, "No empty string") to hide the blocks. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
  $form['blockvisibility_remove_sidebars_php'] = array(
    '#type' => 'textarea',
    '#title' => t('Hide sidebars using PHP'),
    '#default_value' => variable_get('blockvisibility_remove_sidebars_php', ''),
    '#description' => $description,
    '#access' => user_access('use PHP for block visibility'),
  );

  $form['blockvisibility_show_blocks_404'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show blocks on 404 - Page not found pages'),
    '#default_value' => variable_get('blockvisibility_show_blocks_404', 0),
    '#description' => t('Drupal turns off the left and right sidebars on 404 pages. This option allows you to override the default behaviour. The !block404 module provides additional options in handling forms on 404 pages.', array('' => l(t('404 Blocks'), 'http://drupal.org/project/blocks404', array('external' => TRUE)))),
  );

  $form['blockvisibility_show_blocks_403'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show blocks on 403 - Access denied pages'),
    '#default_value' => variable_get('blockvisibility_show_blocks_403', 0),
    '#description' => t('Drupal turns off the left and right sidebars on 403 pages. This option allows you to override the default behaviour.'),
  );

  $form['blockvisibility_update_breadcrumbs'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add 404/403 breadcrumbs'),
    '#default_value' => variable_get('blockvisibility_update_breadcrumbs', 0),
    '#description' => t('Provides a simple breadcrumb trail on these pages.'),
  );

  return system_settings_form($form);
}

